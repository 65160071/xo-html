import java.util.Scanner;
public class xo {
    static char player = 'X';
    static char table[][] = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static int row,col;
    static int x,i = 0;
    public static void showtable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    }
    public static void switchPlayer(){
        if(player == 'X'){
            player = 'O';
        }else{
            player = 'X';
        }
    }
    public static void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        System.out.print("InputRowCol: ");
        row = sc.nextInt();
        col = sc.nextInt();
    }
    public static void pressXO(){
        if(table[row][col] != 'X' && table[row][col] != 'O'){
            table[row][col] = player;
        }else{
            System.out.println("ERROR: Please select a space");
            inputRowCol();
            pressXO();
        }
    }
    static boolean checkX1(){
        if(table[1][1] != '-'){
            if(table[0][0] == table[1][1] && table[1][1] == table[2][2]){
                return true;
            }
        }return false;
    }
    static boolean checkX2(){
        if(table[1][1] != '-'){
            if(table[0][2] == table[1][1] && table[1][1] == table[2][0]){
                return true;
            }
        }return false;
    }
    static boolean checkAllX(){
        checkX1();
        checkX2();
        if(checkX1() || checkX2()){
            return true;
        }return false;
    }
    static boolean checkRow(){
        for(i=0;i<3;i++){
            if(table[i][0] == player && table[i][1] == player && table[i][2] == player){
                return true;
            }
        }
        return false;
    }
    static boolean checkCol(){
        for(i=0;i<3;i++){
            if(table[0][i] == player && table[1][i] == player && table[2][i] == player){
                return true;
            }
        }
        return false;
    }
    public static void cong(){
        if(checkAllX() || checkRow() || checkCol()== true){
            showtable();
            System.out.println("Player "+player+" Win!!");
            x = 1;
        }
    }
    public static void main(String[] args) {
        for(int i=0;i<9;i++){
            showtable();
            inputRowCol();
            pressXO();
            cong();
            if(x == 1){
                break;
            }
            switchPlayer();
            if(i==8){
                showtable();
                System.out.println("Draw!!");
            }
        }
    }
}